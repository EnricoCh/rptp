// library tester project main.go
package main

import (
	"fmt"
	"rtptnet"
)

func main() {
	fmt.Println("Hello World!\b\b\b\b ")
	go func() {

		server := rtptnet.NewServerPsocket()
		//fmt.Println("prima dell'avvio", server.listener)
		server.StartServer()
		//fmt.Println("dopo l'avvio", server.listener)
		psock := server.Accept()
		fmt.Println("client accettato")
		psock.SendMsg([]byte("domandaþ∰█\nþ∰█þ∰█þ∰█"))
		a, _ := psock.Receive()
		fmt.Println(string(a))
	}()

	go func() {

		psock := rtptnet.NewPSocket()
		psock.Connect("127.0.0.1:1717")
		fmt.Println("connessione stabilita")
		psock.SendMsg([]byte("domanda in contemporanea"))
		a, _ := psock.Receive()
		fmt.Println(string(a))

	}()

	fmt.Scanln()
}
