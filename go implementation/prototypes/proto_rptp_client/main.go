// tcp_client project main.go
package main

import (
	"fmt"
	"net"
	"strconv"
)

func main() {
	fmt.Println("Hello World!")
	conn, _ := net.Dial("tcp", "127.0.0.1:1717")
	fmt.Println(conn)

	packet, err := func() ([]byte, error) {
		var size = []byte{}
		var buff = make([]byte, 1)
		var acapo = []byte{10}
		var packet = []byte{}
		var packetbuff = make([]byte, 1)

		for i := 0; i < 10; i++ {
			for {
				n, err := conn.Read(buff)
				if err != nil {
					return packet, err
				}
				if n == 1 {
					break
				}
			}

			fmt.Println("buff ", buff[0], " acapo ", acapo[0])
			if buff[0] == acapo[0] {
				fmt.Println("fine")
				break
			} else {
				fmt.Println("ancora")
				size = append(size, buff[0])
			}

		}
		s := string(size[:])
		n, _ := strconv.Atoi(s)
		fmt.Println(s)
		for i := 0; i < n; i++ {
			for {
				n, _ := conn.Read(packetbuff)
				if n == 1 {
					packet = append(packet, packetbuff[0])
					break
				}
			}
		}
		fmt.Println(packet)
		return packet, nil
	}()

	conn.Close()
	fmt.Println(string(packet), err)

}
