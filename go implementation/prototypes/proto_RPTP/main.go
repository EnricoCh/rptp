// proto_RPTP project main.go
package main

import (
	"fmt"
	"net"
	"strconv"
)

type psocket struct {
	conn net.Conn
}
type serverPsocket struct {
	listener net.Listener
}

func NewServerPsocket() serverPsocket {
	s := serverPsocket{}
	return s
}

func NewPSocket() psocket {
	p := psocket{}
	return p
}

func (pserver *serverPsocket) startServer() {

	listenertmp, _ := net.Listen("tcp", ":1717")
	pserver.listener = listenertmp

}

func (pserver *serverPsocket) accept() psocket {
	conn, _ := pserver.listener.Accept()
	psock := NewPSocket()
	psock.conn = conn
	return psock
}

func (psock *psocket) connect() {
	conn, _ := net.Dial("tcp", "127.0.0.1:1717")
	psock.conn = conn
}

func (psock psocket) sendMsg(msg []byte) {
	numeralsize := len(msg)
	sizestr := strconv.Itoa(numeralsize)
	psock.conn.Write([]byte(sizestr))
	psock.conn.Write([]byte("\n"))
	psock.conn.Write(msg)

}

/*packet, err := */
func (psock psocket) rcv() ([]byte, error) {
	var size = []byte{}
	var buff = make([]byte, 1)
	var acapo = []byte{10}
	var packet = []byte{}
	var packetbuff = make([]byte, 1)

	for i := 0; i < 10; i++ {
		for {
			n, err := psock.conn.Read(buff)
			if err != nil {
				return packet, err
			}
			if n == 1 {
				break
			}
		}

		//fmt.Println("buff ", buff[0], " acapo ", acapo[0])
		if buff[0] == acapo[0] {
			//fmt.Println("fine")
			break
		} else {
			//fmt.Println("ancora")
			size = append(size, buff[0])
		}

	}
	s := string(size[:])
	n, _ := strconv.Atoi(s)
	fmt.Println(s)
	for i := 0; i < n; i++ {
		for {
			n, _ := psock.conn.Read(packetbuff)
			if n == 1 {
				packet = append(packet, packetbuff[0])
				break
			}
		}
	}
	fmt.Println(packet)
	return packet, nil
}

func main() {
	fmt.Println("RPTP tester")
	go func() {

		server := NewServerPsocket()
		//fmt.Println("prima dell'avvio", server.listener)
		server.startServer()
		//fmt.Println("dopo l'avvio", server.listener)
		psock := server.accept()
		fmt.Println("client accettato")
		psock.sendMsg([]byte("domandaþ∰█\nþ∰█þ∰█þ∰█"))
		a, _ := psock.rcv()
		fmt.Println(string(a))
	}()

	go func() {

		psock := NewPSocket()
		psock.connect()
		fmt.Println("connessione stabilita")
		psock.sendMsg([]byte("domanda in contemporanea"))
		a, _ := psock.rcv()
		fmt.Println(string(a))

	}()

	fmt.Scanln()

}
