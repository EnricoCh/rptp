// tcp_server project main.go
package main

import (
	"fmt"
	"net"
	"strconv"
)

func main() {
	fmt.Println("Hello World!")
	listener, _ := net.Listen("tcp", ":1717")
	conn, _ := listener.Accept()
	fmt.Println(conn)

	func(msg []byte) {
		fmt.Println("--")
		numeralsize := len(msg)
		sizestr := strconv.Itoa(numeralsize)
		conn.Write([]byte(sizestr))
		conn.Write([]byte("\n"))
		conn.Write(msg)

	}([]byte("tralllallalallla"))

	conn.Close()

}
