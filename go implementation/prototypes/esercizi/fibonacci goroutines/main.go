// fibonacci goroutines project main.go
package main

import (
	"fmt"
	"sync"
)

func main() {
	fmt.Println("Hello, word")
	var x int
	var y int
	var p string

	x = 1
	y = 2
	p = "weh"

	next(x)
	next(y)
	fmt.Println(p)

	c1 := make(chan int)
	c2 := make(chan int)

	var wg sync.WaitGroup

	wg.Add(3)

	//produce i numeri iniziali
	go func() {
		for i := 0; i < 42; i++ {
			fmt.Println("uno inviato")
			c1 <- i
		}
		close(c1)
		wg.Done()
	}()

	//elabora i numeri
	go func() {
		for msg := range c1 {
			fmt.Println("uno preso")
			c2 <- fib(msg)
		}
		close(c2)
		wg.Done()
	}()

	// consuma i numeri
	go func() {
		for msg := range c2 {
			fmt.Println("eccone uno", msg)
		}
		wg.Done()
	}()

	wg.Wait()

}

func next(x int) int {
	return x + 1
}

func fib(n int) int {
	if n <= 1 {
		return n
	}
	return fib(n-1) + fib(n-2)
}
