// tcp_client project main.go
package main

import (
	"fmt"
	"net"
)

func main() {
	fmt.Println("Hello World!")
	conn, _ := net.Dial("tcp", "127.0.0.1:1717")
	fmt.Println(conn)
	var buff = make([]byte, 1024)
	msg, _ := conn.Read(buff)
	conn.Close()
	fmt.Println(msg)
	fmt.Println(buff)
}
