// tcp_server project main.go
package main

import (
	"fmt"
	"net"
)

func main() {
	fmt.Println("Hello World!")
	listener, _ := net.Listen("tcp", ":1717")
	conn, _ := listener.Accept()
	fmt.Println(conn)
	//var slice = []byte{7, 7, 7, 7, 7, 7, 7, 7}
	conn.Write([]byte("beh\n"))
	conn.Close()
}
