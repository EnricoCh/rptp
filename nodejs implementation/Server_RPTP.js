const net = require('net');
var p_socket = require('./p_socket');
var events = require('events');
var eventEmitter = new events.EventEmitter();


exports.server = class server{

    //port;

    constructor(_port){
        this.port = _port;
    }

    start(conn_handler) {
        net.createServer(function(client){
            console.log("server creato");
            var conn = new p_socket.p_socket(client);
            conn_handler(conn);

        }).listen(this.port);
    }
}

