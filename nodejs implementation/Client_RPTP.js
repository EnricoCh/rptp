const net = require('net');
var p_socket = require('./p_socket');
var events = require('events');
var eventEmitter = new events.EventEmitter();

exports.create_and_connect = function (port,host, conn_handler) {
    var client = new net.Socket();
    client.connect({ port: port, host: host }, function (err){
        if(err) console.log("PROBLEMA");
        console.log("connessione effettuata");
    });
    var conn = new p_socket.p_socket(client); 
    //return conn;
    conn_handler(conn);
}