use std::net::{TcpStream, TcpListener, ToSocketAddrs, SocketAddr};
use std::io::{Read, Write, Error};
use std::time::{Duration, Instant};
use std::thread;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

pub struct RPTPstream {
    connection : TcpStream
}
pub struct RPTPListener{
    listener : TcpListener
}
pub struct RPTPIncoming<'a>{
    conn : & 'a mut RPTPListener
}
impl RPTPListener{  // TODO: untested
    pub fn bind<A: ToSocketAddrs>(addr: A) -> Result<RPTPListener, Error>{
        match TcpListener::bind(addr)  {
            Ok(x) => {
                Ok(RPTPListener { listener: x })
            },
            Err(e) => {
                Err(e)
            },
        }
    }
    pub fn accept(&self) -> Result<(RPTPstream, SocketAddr), Error>{
        let g = self.listener.accept();
        match g {
            Ok(x) => {
                let j = Ok((RPTPstream { connection : x.0}, x.1));
                j
            },
            Err(e) => {
                Err(e)
            },
        }
    }
    pub fn incoming(& mut self) -> RPTPIncoming {   // TODO: incoming è specifico per tcp stream, bisogna crearene uno specicico per rptp? fatto
        RPTPIncoming { conn : self }
    }
}
impl RPTPstream {
    pub fn connect<A: ToSocketAddrs>(addr: A) -> Result<RPTPstream, Error>{
        match TcpStream::connect(addr) {
            Ok(x) => {
                //println!("conn verso il server riuscita");
                Ok(RPTPstream { connection: x})
            },
            Err(e) => {
                //println!("conn verso il server NON riuscita");
                Err(e)
            }
        }
    }
    pub fn send_packet(&mut self, packet : &[u8]){
        let start = Instant::now();
        let full_n = packet.len();
        let preambolo1 = format!("{}\n", full_n);
        let preambolo= preambolo1.as_bytes();
        let mut current: usize = 0;
        loop{
            current += self.connection.write(preambolo).unwrap();
            if current == preambolo.len(){
                break;
            }
        }
        current = 0;
        loop{
            current += self.connection.write(packet).unwrap();
            if current == packet.len(){
                break;
            }
        }
        let end = start.elapsed();
        println!("data sent in {}", end.as_millis());
    }
    pub fn receive_packet(&mut self) -> Vec<u8> {
        let start = Instant::now();
        let mut packet_size: String = "".parse().unwrap();
        loop{
            let mut data = [0 as u8; 1]; // using 50 byte buffer
            let read_n = self.connection.read(&mut data).unwrap();
            match read_n {
                0 => {
                    println!("niente");
                    thread::sleep(Duration::from_millis(100))
                }
                1 => {
                    match data {
                        [10] => {
                            break;
                        }
                        ([x]) => {
                            let c = x as char;
                            match c.is_digit(10){
                                true => {
                                    packet_size.push(c);
                                }
                                false => {
                                    println!("errore nel carattere {} : mi aspettavo un digit", c);
                                    panic!()
                                }
                            }
                        }
                    }
                }
                _ => {
                    panic!();
                }
            }
        }
        let size = packet_size.parse::<i32>().unwrap() as usize;
        let mut buffer = vec![0; size];
        self.connection.read_exact(&mut buffer[..]).expect("read_exact failed");
        let end = start.elapsed();
        println!("data received in {}", end.as_millis());
        buffer
    }
}
impl<'a> Iterator for RPTPIncoming<'a>{
    type Item = RPTPstream;

    fn next(&mut self) -> Option<Self::Item> {
        //println!("next");
        let next_conn = self.conn.listener.incoming().next().expect("errore");
        Option::from(RPTPstream { connection: next_conn.expect("errore") })
    }
}

