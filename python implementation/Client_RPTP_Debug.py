import socket
import json


class client_p_socket:
    def create(*, host, port):
        HOST = host
        PORT = port
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.connect((host,port))
        _tunnel = p_socket(serversocket)
        return _tunnel



class p_socket :

    def __init__(self,clientsocket):
        self.clientsocket = clientsocket

    def snd_packet(self, data):
        packet = str(len(data))
        packet += "\n"
        self.clientsocket.send(packet.encode('utf-8'))
        self.clientsocket.send(data)

    def rcv_packet(self):
        str_size = ""
        while True:
            data = self.clientsocket.recv(1).decode('utf-8')
            print(" > ",data, sep = "", end = "")
            if data == '\n':
                break
            elif not data.isdigit():
                print("PROBLEMA",ord(data))
                print("PROBLEMA")
                break
            else:
                str_size += data
        size = int(str_size)
        print(size)
        pack = bytes([])
        for count in range (0,size) :
            pack += self.clientsocket.recv(1)
        return pack



#tunnel = client_p_socket.create(host='127.0.0.1',port=9900)
#print(tunnel.rcv_packet().decode('utf-8'))


