import java.io.IOException;
import java.nio.charset.Charset;

public class Client_main {

    public static void main(String[] args) throws IOException {

        ClientPSocket cps = new ClientPSocket();
        PSocket p = cps.connect("127.0.0.1", 9900);
        byte[] dati = p.rcv();
        System.out.println("rcved " + new String(dati, Charset.forName("UTF-8")));

    }
}
