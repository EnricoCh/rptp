import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.charset.Charset;
import java.util.Arrays;

public class main {

    private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    public static void main(String[] args){
        ServerPSocket p = null;
        PSocket ps = null;
        try {
             p = new ServerPSocket(9900);
             System.out.println("ServerPSocket creata");
             ps = p.accept();
             ps.send("ciao".getBytes(StandardCharsets.UTF_8));
            System.out.println("PSocket accettata");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

//useless methods needed to understand encoding only
    static String decodeUTF8(byte[] bytes) {
        System.out.println("decoded to ->" + new String(bytes, UTF8_CHARSET));
        return new String(bytes, UTF8_CHARSET);
    }

    static byte[] encodeUTF8(String string) {
        System.out.println("encoded to ->" + string.getBytes(UTF8_CHARSET).toString());
        System.out.println("encoded to ->" + Arrays.toString(string.getBytes(UTF8_CHARSET)));
        return string.getBytes(UTF8_CHARSET);
    }


}
